import pandas as pd
import requests
import os

# scroll down to the bottom to implement your solution

if __name__ == '__main__':

    if not os.path.exists('../Data'):
        os.mkdir('../Data')

    # Download data if it is unavailable.
    if ('A_office_data.xml' not in os.listdir('../Data') and
            'B_office_data.xml' not in os.listdir('../Data') and
            'hr_data.xml' not in os.listdir('../Data')):
        print('A_office_data loading.')
        url = "https://www.dropbox.com/s/jpeknyzx57c4jb2/A_office_data.xml?dl=1"
        r = requests.get(url, allow_redirects=True)
        open('../Data/A_office_data.xml', 'wb').write(r.content)
        print('Loaded.')

        print('B_office_data loading.')
        url = "https://www.dropbox.com/s/hea0tbhir64u9t5/B_office_data.xml?dl=1"
        r = requests.get(url, allow_redirects=True)
        open('../Data/B_office_data.xml', 'wb').write(r.content)
        print('Loaded.')

        print('hr_data loading.')
        url = "https://www.dropbox.com/s/u6jzqqg1byajy0s/hr_data.xml?dl=1"
        r = requests.get(url, allow_redirects=True)
        open('../Data/hr_data.xml', 'wb').write(r.content)
        print('Loaded.')

        # All data in now loaded to the Data folder.

# Import data from xml files
data_a_office = pd.read_xml('../Data/A_office_data.xml')
data_b_office = pd.read_xml('../Data/B_office_data.xml')
data_hr = pd.read_xml('../Data/hr_data.xml')

# Update the index on the dataframes
data_a_office['index'] = 'A' + data_a_office['employee_office_id'].map(str)
data_a_office.set_index('index', inplace=True)

data_b_office['index'] = 'B' + data_b_office['employee_office_id'].map(str)
data_b_office.set_index('index', inplace=True)

data_hr['index'] = data_hr['employee_id'].map(str)
data_hr.set_index('index', inplace=True)

# Combine data for offices A and B into one dataframe
data_office = pd.concat([data_a_office, data_b_office])

# Merge HR data into office dataframe
data = pd.merge(data_office, data_hr, left_index=True, right_index=True)

# Drop the extra columns
data.drop(columns=['employee_id', 'employee_office_id'], inplace=True)

# Sort the dataframe by the index
data.sort_index(inplace=True)


# Create function that counts the number of employees who worked on more than 5 projects
def count_bigger_5(series):
    return len(series[series > 5])


# What are the departments of the top ten employees in terms of working hours?
print(list(data.sort_values('average_monthly_hours', ascending=False)['Department'].head(10)))

# What is the total number of projects on which IT department employees with low salaries have worked?
print(data.query("Department == 'IT' & salary == 'low'")['number_project'].sum())

# What are the last evaluation scores and the satisfaction levels of the employees A4, B7064, and A3033?
print(data.loc[['A4', 'B7064', 'A3033']][['last_evaluation', 'satisfaction_level']].values.tolist())

# What is the median number of projects the employees in a group worked upon,
# and how many employees worked on more than 5 projects?
print(data.groupby('left').agg({'number_project': ['median', count_bigger_5]}))

# What is the mean and median time spent in the company?
print(data.groupby('left').agg({'time_spend_company': ['mean', 'median']}).round(2))

# What is the share of employees who've had work accidents?
print(data.groupby('left').agg({'Work_accident': ['mean']}).round(2))

# What is the mean and standard deviation of the last evaluation score?
print(data.groupby('left').agg({'last_evaluation': ['mean', 'std']}).round(2))

# Display median number of hours employees have worked per month by
# departments and employee's current status and their salary level
hours_worked = pd.pivot_table(data,
                              values='average_monthly_hours',
                              index='Department',
                              columns=['left', 'salary'],
                              aggfunc='median'
                              )
hours_worked_filter = hours_worked[(hours_worked[(0, 'high')] < hours_worked[(0, 'medium')]) |
                                   (hours_worked[(1, 'low')] < hours_worked[(1, 'high')])]
print(hours_worked_filter.to_dict())

# Display the min, mean, and max last evaluation score and satisfaction level by
# an employee's time in the company and whether an employee has had any promotion
eval_score = pd.pivot_table(data,
                            values=['satisfaction_level', 'last_evaluation'],
                            index='time_spend_company',
                            columns='promotion_last_5years',
                            aggfunc=['min', 'mean', 'max']
                            )
eval_score_filter = eval_score[eval_score[('mean', 'last_evaluation', 0)] > eval_score[('mean', 'last_evaluation', 1)]]
print(eval_score_filter.to_dict())
